FROM nvidia/opengl:base-ubuntu20.04

RUN apt-get update -yqq && apt-get install python3 pip openjdk-11-jdk git build-essential curl --no-install-recommends -y && apt-get clean
RUN pip install ue4cli

RUN groupadd android && useradd -d /opt/android-sdk-linux -g android android
COPY --from=cijumbo/unreal:android_30 --chown=android:android /opt/ /opt/
#RUN chown -R android:android /opt/

ENV DEBIAN_FRONTEND   noninteractive
ENV ANDROID_HOME      /opt/android-sdk-linux
ENV ANDROID_SDK_HOME  ${ANDROID_HOME}
ENV ANDROID_SDK_ROOT  ${ANDROID_HOME}
ENV ANDROID_SDK       ${ANDROID_HOME}
ENV PATH "${PATH}:${ANDROID_HOME}/cmdline-tools/latest/bin"
ENV PATH "${PATH}:${ANDROID_HOME}/cmdline-tools/tools/bin"
ENV PATH "${PATH}:${ANDROID_HOME}/tools/bin"
ENV PATH "${PATH}:${ANDROID_HOME}/build-tools/30.0.3"
ENV PATH "${PATH}:${ANDROID_HOME}/platform-tools"
ENV PATH "${PATH}:${ANDROID_HOME}/emulator"
ENV PATH "${PATH}:${ANDROID_HOME}/bin"
ENV NDKINSTALLPATH    ${ANDROID_HOME}/ndk/21.1.6352462
ENV NDKROOT           $NDKINSTALLPATH
ENV NDK_ROOT          $NDKINSTALLPATH
ENV PATH "${PATH}:/home/runner/UnrealEngine/Engine/Binaries/Linux/"

# UE4 Setup
## Copy Rehago folder as long as we are still root
RUN mkdir -p /home/runner
RUN chown -R android:android /home/runner

USER android

ENV UNREAL_DIR /home/runner/UnrealEngine
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64

COPY --from=cijumbo/unreal:source-v5.1.0 --chown=android:android /UnrealEngine/ /home/runner/UnrealEngine

WORKDIR /home/runner/UnrealEngine
RUN git config --global --add safe.directory /home/runner && git stash & git reset && git checkout -f 4.27

#Execute the Unreal Engine setup script to set up the files needed to generate project files
RUN ./Setup.sh
#Generate the project files
RUN ./GenerateProjectFiles.sh && make CrashReportClient ShaderCompileWorker UnrealLightmass UnrealPak UE4Editor ARGS="-clean"

WORKDIR /home/

RUN ue4 setroot /home/runner/UnrealEngine
# It might be helpful to compile project once with "-compile" option so this doesn't have to be done with every build
