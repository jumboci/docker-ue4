# UE-Android in Docker

This project is intended to create linux based containers with Unreal Engine Android targets. It's a special purpose container for building Android applications. If you just want to run tests in a container, you're probably better off with the [official Unreal containers](https://docs.unrealengine.com/latest/en-US/overview-of-containers-in-unreal-engine/).

# Prerequisites

- A container management software capable of running linux containers such as [podman](https://podman.io/), [Rancher](https://rancherdesktop.io/) or [Docker](https://www.docker.com/). Ideally, you would set up your build machine as a GitLab runner, so you can harness the power of repeatable steps and don't have to worry about timed logouts or stale shell windows on your local machine.
- Access to https://github.com/EpicGames/UnrealEngine in order to build Unreal from source. [Learn more](https://docs.unrealengine.com/latest/en-US/downloading-unreal-engine-source-code/)
- [Suggested] Access to a container registry to push your image to. You could also use the images only locally, if you don't intend to use a second runner.

# Installation

Do yourself a favour and set up your build machine as a (GitLab) runner: https://docs.gitlab.com/runner/install/. You will be able to use that same runner for your Android builds later. Make sure to set the timeouts in the `config.toml` of your runner and your project settings (Settings / CI/CD / General pipelines) to at least 24h to allow for long build times.

# Usage

Once everything is set up, you can specify the versions you need for your build

# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update test your changes first.

# Roadmap

- Pass GitHub Token by the suggested mount type "secret" [link](https://docs.docker.com/engine/reference/builder/#run---mounttypesecret)
- Make builds use variables for all version numbers
- Test with UE5.2 and latest Android version
- Make use of [docker-ue4](https://github.com/adamrehn/ue4-docker) with `--prerequisites-dockerfile` and `--opt buildgraph_args` for Android

# License

This project is available under the MIT License.

Unreal and its logo are Epic Games' trademarks or registered trademarks in the US and elsewhere.

Docker and the Docker logo are trademarks or registered trademarks of Docker in the United States and other countries.